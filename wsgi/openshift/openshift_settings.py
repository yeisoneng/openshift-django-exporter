import sys
import os
import importlib

# Project parent
BASE_OPENSHIFT_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

# Need config
sys.path.append(BASE_OPENSHIFT_DIR)
import config as CONFIG

# Need settings
sys.path.append(os.path.join(BASE_OPENSHIFT_DIR, "django_project"))
sys.path.append(os.path.join(BASE_OPENSHIFT_DIR, "django_project", CONFIG.NAME))
from settings import *


# We are on openshift server?
ON_OPENSHIFT = False
if 'OPENSHIFT_REPO_DIR' in os.environ:
    ON_OPENSHIFT = True

# Ovwerwrite Secret key
if ON_OPENSHIFT:
    try:
        SECRET_KEY = os.environ['DJANGO_SETTINGS_SECRET_KEY']
    except KeyError:
        print("Please create env variable DJANGO_SETTINGS_SECRET_KEY (cf README)")
else:
    SECRET_KEY = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz' # dev key


if ON_OPENSHIFT:
    DEBUG = False
else:
    DEBUG = True

DEBUG = True


if DEBUG:
    ALLOWED_HOSTS = []
else:
    ALLOWED_HOSTS = ['*']


WSGI_APPLICATION = 'openshift.wsgi.application'


# PostreSQl database
if ON_OPENSHIFT:
    DATABASES = {
        'default': {
                     'ENGINE':   'django.db.backends.postgresql_psycopg2',
                     'NAME':     os.environ['OPENSHIFT_APP_NAME'],
                     'USER':     os.environ['OPENSHIFT_POSTGRESQL_DB_USERNAME'],
                     'PASSWORD': os.environ['OPENSHIFT_POSTGRESQL_DB_PASSWORD'],
                     'HOST':     os.environ['OPENSHIFT_POSTGRESQL_DB_HOST'],
                     'PORT':     os.environ['OPENSHIFT_POSTGRESQL_DB_PORT'],
                     }
    }


# Collectstatic
if ON_OPENSHIFT:
    STATIC_ROOT = os.path.join(os.environ['OPENSHIFT_REPO_DIR'], 'wsgi', 'static')
else:
    STATIC_ROOT = os.path.join(BASE_OPENSHIFT_DIR, "wsgi", "static")



STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.FileSystemFinder',
)



