import os
import sys
import shutil

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
REPO_DIR = os.path.join(BASE_DIR, "django_project")
sys.path.append(BASE_DIR)


# os.environ["OPENSHIFT_DATA_DIR"] = os.path.join(BASE_DIR, "OPENSHIFT_DATA_DIR")


import config as CONFIG

if __name__ == "__main__":

    if hasattr(CONFIG, "MAIN"):

        if os.path.exists(REPO_DIR):
            shutil.rmtree(REPO_DIR)

        zip_ = os.path.join(BASE_DIR, "default.zip")
        if os.path.exists(zip_):
            os.remove(zip_)

        print("Downloading {} as {}".format(CONFIG.MAIN[0], zip_))
        os.system("wget {} -O {}".format(CONFIG.MAIN[0], zip_))
        print("Unzip {}".format(zip_))
        os.system("unzip {}".format(zip_))

        name = list(filter(lambda s:s.startswith(CONFIG.MAIN[1]), os.listdir(os.path.dirname(os.path.abspath(__file__)))))[0]
        old = os.path.join(os.path.dirname(os.path.abspath(__file__)), name)

        os.system("Moving {} to {}".format(old, REPO_DIR))
        os.system("mv {} {}".format(old, REPO_DIR))

        os.system("Removing {}".format(zip_))
        os.system("rm {}".format(zip_))




    if hasattr(CONFIG, "DATA_DIR"):

        print("Downloading {}".format(CONFIG.DATA_DIR[0]))

        for elem in os.listdir(os.getenv("OPENSHIFT_DATA_DIR")):
            x = os.path.join(os.getenv("OPENSHIFT_DATA_DIR"), elem)
            if os.path.isdir(x):
                shutil.rmtree(x)
            else:
                os.remove(x)

        zip_ = os.path.join(os.getenv("OPENSHIFT_DATA_DIR"), "default.zip")
        if os.path.exists(zip_):
            os.remove(zip_)

        os.system("wget {} -O {}".format(CONFIG.DATA_DIR[0], zip_))
        os.system("unzip {}".format(zip_))

        name = list(filter(lambda s:s.startswith(CONFIG.DATA_DIR[1]), os.listdir(os.path.join(BASE_DIR, "wsgi"))))[0]
        old = os.path.join(BASE_DIR, "wsgi", name)

        for f in os.listdir(old):
            old_n = os.path.join(old, f)

            os.system("mv {} {}".format(old_n, os.getenv("OPENSHIFT_DATA_DIR")))

        os.system("rm -r {}".format(old))
        os.system("rm {}".format(zip_))
