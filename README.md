Openshift, Django1.8 and Python3.3
====================================


This repository is a tool for export existing Django projects to [OpenShift](https://openshift.redhat.com/).

Features
--------
* No need extensive configuration
* Clone project in every restart, so you don't need work on more that one repository ;)

To-do
--------
* Is possible convert this in one simple Django application?, and just add to INSTALLED_APPS


TL;DR
-----

```
#!bash
gem install rhc
rhc setup
rhc app create -a mynewapp -t python-3.3
cd mynewapp
git remote add upstream -m master git@bitbucket.org:yeisoneng/openshift-django-exporter.git
git pull -s recursive -X theirs upstream master
python -c "import random;print('SECRET_KEY: ' + ''.join([random.choice('abcdefghijklmnopqrstuvwxyz0123456789') for n in range(60)]))"
rhc set-env DJANGO_SETTINGS_SECRET_KEY="{SECRET_KEY}" -a mynewapp
rhc cartridge-add postgresql-9.2 --app mynewapp
git push
rhc ssh mynewapp
source $OPENSHIFT_HOMEDIR/python/virtenv/venv/bin/activate
python "$OPENSHIFT_REPO_DIR"wsgi/manage.py createsuperuser
```


Prerequisites
-------------

You need to have an OpenShift account: if not, [create a new one](https://www.openshift.com/app/account/new).

Secondly, you need to install and configure `rhc` tools on your computer:

    gem install rhc
    rhc setup

Thirdly, you need to create a new OpenShift application with Python 3.x cartridge:

* By web interface: [new python 3.3 app](https://openshift.redhat.com/app/console/application_type/cart!python-3.3) + git clone
* By command line: `rhc app create -a mynewapp -t python-3.3`

Add django enabler
------------------

Run the following commands to add the current project to the app repository:

    cd mynewapp
    git remote add upstream -m master git@bitbucket.org:yeisoneng/openshift-django-exporter.git
    git pull -s recursive -X theirs upstream master

Configuration
-------------

Django "secret key" is already set for localhost usage. For production/OpenShift cartridge, you'll have to set a new secret one.

### Generate a new secret key

Django "secret key" is already set for localhost usage. For production/OpenShift cartridge, you'll have to set a new secret one.

You can use a web service like: [Django Secret Key Generator](http://www.miniwebtool.com/django-secret-key-generator/)
Or by using python command line:

    import random
    print(''.join([random.choice('abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)') for n in range(60)]))

### Push key to the cartridge

When key is generated, you need to set an environment variables on the OpenShift cartridge:

    rhc set-env DJANGO_SETTINGS_SECRET_KEY="your-secret-key" -a mynewapp

### Database backend

In `mynewapp/openshift/settings.py`, MySQL backend is configured. If you whant to use one, you'll need to have a database cartridge to your app. Examples:

MySQL:

    rhc cartridge-add mysql-5.5 --app mynewapp
    rhc cartridge-add phpmyadmin-4 --app mynewapp

PostgreSQL:

    rhc cartridge-add postgresql-9.2 --app mynewapp

Then you may have to adapt settings according to your database backend (with the help of OpenShift ['Using Environment Variables' documentation](https://developers.openshift.com/en/managing-environment-variables.html)):

```
# PostgreSQL
if ON_OPENSHIFT:
    DATABASES = {
         'default': {
            'ENGINE':   'django.db.backends.mysql',
            'NAME':     os.environ['OPENSHIFT_APP_NAME'],
            'USER':     os.environ['OPENSHIFT_MYSQL_DB_USERNAME'],
            'PASSWORD': os.environ['OPENSHIFT_MYSQL_DB_PASSWORD'],
            'HOST':     os.environ['OPENSHIFT_MYSQL_DB_HOST'],
            'PORT':     os.environ['OPENSHIFT_MYSQL_DB_PORT'],
         }
    }
```

### First push
Simply:

    git push

Django super user
-----------------
After first push, your application do not have a super user (admin). You have to create one:

    rhc ssh mynewapp
    source $OPENSHIFT_HOMEDIR/python/virtenv/venv/bin/activate
    python "$OPENSHIFT_REPO_DIR"wsgi/manage.py createsuperuser